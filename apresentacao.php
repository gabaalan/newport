<div class="apresentacao">
	
	<h1>Newport Residence</h1>
	
	<p>O Newport resgata o melhor da construção tradicional de alto padrão. Com uma construção mais sólida e acabamentos de alto requinte e durabilidade, ele foi projetado para ultrapassar a barreira do tempo. São particularidades que encantam pela sofisticação e conforto em um ambiente único e especial para morar.</p>
	
	<p>Desvende os segredos ocultos em cada um dos detalhes do Newport. São particularidades que encantarão todos os seus sentidos e trarão sofisticação e prazer em morar em um ambiente único e especial. </p>
	
	<h5>Muitas coisas que realmente importam na sua vida não são visíveis a olho nu. Até onde seus olhos podem ver?</h5>
	
	<a href="teste.php" class="load-page">Conheça os diferenciais Newport</a>
	
</div>