<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Newport Residence</title>

	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="fonts/helvetica/helvetica.css" rel="stylesheet" media="screen">
    <link href="fonts/helveticaneue/helveticaneue.css" rel="stylesheet" media="screen">
    <link href="css/geral.css" rel="stylesheet" media="screen">

</head>
<body>

	<div class="back">
		<div class="loader">
			<img src="img/ajax-loader.gif" alt="Carregando" />
		</div>
		
		<div class="images">
			<img class="current img-responsive relative" src="img/fundos/inicial.jpg" alt="Newport Residence" />
		</div>
	</div>
	
	<div class="head site-align">
		
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<!-- MUDANÇA: div row adicionada -->
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<div class="row">
			
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<!-- MUDANÇA: div col-logo adicionada -->
			<!-- Para a logo ficar um pouco maior em resoluções menores, pode usar as classes abaixo no lugar das atuais -->
			<!-- col-lg-6 col-md-5 col-sm-4 col-xs-3 col-logo --> <!-- Mudar na div col-logo -->
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<div class="col-lg-6 col-md-4 col-sm-3 col-xs-3 col-logo">
				<div class="logo">
					<h1>
						<a href="img/fundos/inicial.jpg" class="background-change">
							<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
							<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
							<!-- MUDANÇA: Antes havia apenas texto, agora é uma tag img com a nova imagem: logoimage.png -->
							<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
							<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
							<img src="img/logoimage.png" alt="Newport Residence" class="img-responsive" />
						</a>
					</h1>
				</div>
			</div> <!-- +++++++++++++++++ final da div col-logo adicionado +++++++++++++++++ -->
			
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<!-- MUDANÇA: div col-thumbs adicionada -->
			<!-- Para a logo ficar um pouco maior em resoluções menores, pode usar as classes abaixo no lugar das atuais -->
			<!-- col-lg-6 col-md-7 col-sm-8 col-xs-9 col-thumbs --> <!-- Mudar na div col-thumbs -->
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
			<div class="col-lg-6 col-md-8 col-sm-9 col-xs-9 col-thumbs">
				<div class="thumbs pull-right" data-delay="10000"> <!-- Delay == milisegundos para passar de imagem - 1000 = 1s -->
					<ul>
						<li class="active"><a href="img/fundos/inicial.jpg" class="background-change" style="background-image: url(img/fundos/thumb/inicial.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/fitness.jpg" class="background-change" style="background-image: url(img/fundos/thumb/fitness.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/hall.jpg" class="background-change" style="background-image: url(img/fundos/thumb/hall.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/brinquedoteca.jpg" class="background-change" style="background-image: url(img/fundos/thumb/brinquedoteca.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/living.jpg" class="background-change" style="background-image: url(img/fundos/thumb/living.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/sacada.jpg" class="background-change" style="background-image: url(img/fundos/thumb/sacada.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/salao.jpg" class="background-change" style="background-image: url(img/fundos/thumb/salao.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/spa.jpg" class="background-change" style="background-image: url(img/fundos/thumb/spa.jpg);"><span></span> Brinquedoteca</a></li>
						<li><a href="img/fundos/suite.jpg" class="background-change" style="background-image: url(img/fundos/thumb/suite.jpg);"><span></span> Brinquedoteca</a></li>
					</ul>
				</div>
			</div> <!-- +++++++++++++++++ final da div col-thumbs adicionado +++++++++++++++++ -->
			
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<!-- MUDANÇA: final div row adicionada -->
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
		</div>
		
	</div>
	
	<div class="content site-align" id="content">
		<a href="javascript:;" id="close">X</a>
		<div id="html">
			
		</div>
	</div>
	
	<div class="description site-align">
		<h3>Hall Decorado</h3>

		<p>Com pé-direito duplo, decorado e pisos revestidos com pedras nobres, você terá, logo na entrada, sensações de amplitude e sofisticação.</p>
	</div>
	
	<div class="menu site-align">
		
		<ul class="navigation">
			<li>
				<a href="javascript:;">Projeto</a>
				<ul class="sub-navigation">
					<li class="title"><a href="apresentacao.php" class="load-page">Apresentação</a></li>
					<li class="divider"></li>
					<li class="title"><a href="teste.php" class="load-page">Diferenciais Newport</a></li>
				</ul>
			</li>
			<li>
				<a href="javascript:;">Perspectivas</a>
				<ul class="sub-navigation">
					<li class="title"><h5>Áreas Comuns</h5></li>
					<li><a href="img/fundos/hall.jpg" class="background-change" data-title="Hall Decorado" data-description="Com pé-direito duplo, decorado e pisos revestidos com pedras nobres, você terá, logo na entrada, sensações de amplitude e sofisticação.">Hall de entrada</a></li>
					<li><a href="img/fundos/salao.jpg" class="background-change">Salão de festas</a></li>
					<li><a href="img/fundos/brinquedoteca.jpg" class="background-change">Brinquedoteca</a></li>
					<li><a href="img/fundos/fitness.jpg" class="background-change">Fitness</a></li>
					<li><a href="img/fundos/inicial.jpg" class="background-change">Piscina</a></li>
					<li><a href="img/fundos/spa.jpg" class="background-change">Spa</a></li>
					<li class="divider"></li>
					<li class="title"><h5>Áreas Privativas</h5></li>
					<li><a href="img/fundos/sacada.jpg" class="background-change">Sacada gourmet</a></li>
					<li><a href="img/fundos/living.jpg" class="background-change">Living</a></li>
					<li><a href="img/fundos/suite.jpg" class="background-change">Suíte máster</a></li>
				</ul>
			</li>
			<li><a href="javascript:;">Plantas</a></li>
			<li><a href="javascript:;">Localização</a></li>
			<li><a href="javascript:;">Construção</a></li>
			<li><a href="javascript:;">Downloads</a></li>
			<li><a href="javascript:;">Contato</a></li>
		</ul>
		 
		<div class="address">
			<address class="phone pull-right">
				47 <b>3028-7473</b>
			</address>
			<address>
				<a href="http://www.google.com.br/">Axia Participações e Construções</a>
			</address>
		</div>
	</div>
	
	<!-- Scripts -->
    <script type="text/javascript" src="js/jquery.1.10.2.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/geral.js"></script>
</body>
</html>