Site Newport Residence
=======================

Virtual Host
------------

	NameVirtualHost newport.com:80

	<Directory "dir/projetos/Newport">
		Options All
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>

	<VirtualHost newport.com:80>
		ServerName newport.com
		ServerAlias www.newport.com
		DocumentRoot "dir/projetos/Newport"
	</VirtualHost>

