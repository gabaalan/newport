
var rollInterval = null;

$(function(){
	
	delegateActions();
	
	$('#content a#close').on('click', closePage);
	
	$(window).on('resize', mudaTela);
	
	setTimeout(
		function()
		{
			mudaTela( null );
		},
		50
	);
		
	beginRoll();
	
});

function beginRoll()
{
	clearTimeout( rollInterval );
	
	var delay = $('.head .thumbs').data('delay');
	
	if( !delay )
	{
		delay = 10000;
		
		$('.head .thumbs').data('delay', 10000);
	}
	
	//var quantidade = $('.head .thumbs ul li').length;
	
	var active = $('.head .thumbs ul li.active');
	
	rollInterval = setTimeout(
		function()
		{
			var next = active.next();
			
			if( next.length <= 0 )
			{
				next = $('.head .thumbs ul li').get(0);
			}
			
			$(next).find('a').trigger('click');
			
			beginRoll();
		},
		delay
	);
}

function delegateActions()
{
	$('a.background-change').on('click', backgroundChange);
	$('a.load-page').on('click', loadPage);
}

function closePage( evento )
{
	$('#content').removeClass('show');
	
	if( $('.description').hasClass('return') )
	{
		$('.description').removeClass('return');
		$('.description').addClass('show');
	}
}

function loadPage( evento )
{
	$('.back .loader').addClass('active');
	
	$('#content').removeClass('show');
	
	if( $('.description').hasClass('show') )
	{
		$('.description').removeClass('show');
		$('.description').addClass('return');
	}
	
	var href = $(this).attr('href');
	
	setTimeout(
		function()
		{
			$('#content #html').load(
				href,
				function( response, status, xhr )
				{
					$('#content').addClass('show');

					$('.back .loader').removeClass('active');

					delegateActions();
				}
			);
		},
		500 //Tempo da animação de saida
	);
		
	evento.stopPropagation();
	evento.preventDefault();
	
	return false;
}

function mudaTela( evento )
{	
	var width = $(window).width();
	var height = $(window).height();
	
	var image = $('.back .images img.current');
	
	var imageWidth = image.width();
	var imageHeight = image.height();
	
	if( imageHeight < height && imageWidth >= width )
	{
		$('.back .images').css('height', height + 'px');
		
		image.css('max-width', 'none');
		image.css('width', 'auto');
		image.css('max-height', height + 'px');
	}
	else if( ( imageHeight >= height && imageWidth < width ) || ( imageHeight < height && imageWidth < width ) )
	{
		$('.back .images').css('height', imageHeight + 'px');
		
		image.css('max-height', 'none');
		image.css('height', 'auto');
		image.css('max-width', '100%');
	}
	else
	{
		$('.back .images').css('height', imageHeight + 'px');
	}
}

function backgroundChange( evento )
{
	var href = $(this).attr('href');
	
	var title = $(this).data('title');
	var description = $(this).data('description');
	
	$('.description').removeClass('return');
	
	if( title && description )
	{
		$('.description h1').text( title );
		$('.description p').text( description );
		
		if( $('#content').hasClass('show') )
		{
			$('.description').addClass('return');
		}
		else
		{
			$('.description').addClass('show');
		}
	}
	else
	{
		$('.description').removeClass('show');
	}
	
	carregaFoto( href );
	
	evento.stopPropagation();
	evento.preventDefault();
	
	return false;
}

function mudaFoto( image )
{	
	$('.back .images .current').removeClass('current').addClass('prev');

	$(image).css('opacity', 0);
	$(image).addClass('current');
	$(image).removeClass('prev');

	$('.back .images').append( image );

	$('.back .loader').removeClass('active');

	setTimeout(
		function()
		{
			$('.head .thumbs ul li.active').removeClass('active');
			
			var thumbs = $('.head .thumbs ul li a');

			thumbs.each(
				function( index, value )
				{					
					if( $(image).attr('src') == $(value).attr('href') )
					{
						$(value).parent().addClass('active');
					}
				}	
			);
			
			$('.back .images .current').css('opacity', 1);
			
			beginRoll();

			//$('.back .images').css('height', image.height + 'px');
			
			mudaTela( null );

//			setTimeout(
//				function()
//				{
//					$('.back .prev').addClass('hidden');
//				},
//				500 // Tempo da animação CSS
//			);
		},
		50
	);
}

function carregaFoto( path )
{
	$('.back .loader').addClass('active');

	var image = new Image();

	var src = path;

	$(image).addClass('img-responsive');

	var images = $('.back .images img');

	var existing = null;

	var itscurrent = false;

	images.each(
		function( index, value )
		{
			if( src == $(value).attr('src') )
			{
				if( $(value).hasClass('current') )
					itscurrent = true;
				
				existing = value;
			}
		}	
	);
		
	if( itscurrent )
	{
		$('.back .loader').removeClass('active');
		
		return true;
	}

	if( existing )
	{		
		mudaFoto( existing );
		
		return true;
	}

	$(image)
		.load(
			function() 
			{				
				mudaFoto( this );
			}
		)
		.error( 
			function( jqXHR, data, errorThrown ) 
			{
				console.error( 'Não foi possível carregar imagem: ' + src );
			}
		);
			
	$(image).attr('src', src);

	return true;
}